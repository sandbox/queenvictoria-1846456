IMAGE LINKSTYLE FORMATTER
-----------------------

This is a direct copy of the image_caption_formatter
	http://drupal.org/project/image_caption_formatter

I've removed the captions and added the styles. It could do with some error handling (check that the style exists before trying to use it or return File on failure).